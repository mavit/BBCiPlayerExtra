package Plugins::BBCiPlayerExtra::BBCExtraXMLParser;

# Plugin to play live and on demand BBC radio streams
# (c) Triode, 2007-2015, triode1@btinternet.com
#
# Released under GPLv2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;

use Slim::Utils::Log;
use Slim::Utils::Prefs;
use XML::Simple;
use Date::Parse;
use URI::Escape qw(uri_escape_utf8 uri_unescape);

use Data::Dumper;

my $log = logger('plugin.bbciplayerextra');
my $prefs = preferences('plugin.bbciplayerextra');
my $prefsplayer = preferences('plugin.bbciplayer');


sub parse {
    my $class  = shift;
    my $http   = shift;
    my $optstr = shift;

    my $params = $http->params('params');
    my $url    = $params->{'url'};
	
	my $imageres = $prefsplayer->get('imageres');

	my $now = time();
	
	my $xml = eval {
		XMLin( 
			$http->contentRef,
#			KeyAttr    => { parent => 'type', link => 'transferformat', image => 'entity_type' },
			KeyAttr    => { parent => 'type', image => 'entity_type' },
			GroupTags  => { links => 'link', parents => 'parent' },
			ForceArray => [ 'parent', 'link' ]
		)
	};

	if ($@) {
		$log->error("$@");
		return;
	}

	my @months   = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
	my @weekdays = qw(Sunday Monday Tuesday Wednesday Thursday Friday Saturday);
	my $today = (localtime($now))[3];

	my %byKey;
	my %list;
 
	my %filter;
	my %programmeSeen; # Used to eliminate programme repeats
	
	# parse xml response into menu entries
	
	ENTRY: for my $entry (@{$xml->{'entry'}}) {

		my $title = $entry->{'title'};  
		my $start = str2time($entry->{'broadcast'}->{'start'});
		my ($min, $hour, $day, $wday) = (localtime($start))[1,2,3,6];
		
		# move info to top level of entry so we can filter on it
#		$entry->{'hlsurl'}  = $entry->{'links'}->{'hls'}->{'content'};
#		$entry->{'dashurl'} = $entry->{'links'}->{'dash'}->{'content'};
		for my $info (keys %{$entry->{'parents'}}) {
			$entry->{ lc($info) } = $entry->{'parents'}->{ $info }->{'content'};
		}

		if ($start > $now ) {
			next ENTRY;
		}

		my $availStart = str2time($entry->{'availability'}->{'start'});
		my $availEnd   = str2time($entry->{'availability'}->{'end'});

		# don't include if the program is not available now (feed includes programs which can't be played)
		
		
#		if ($availStart > $now || $availEnd < $now) {
#			$log->error("ignoring $title [start=$availStart end=$availEnd now=$now]");
#			next ENTRY;
#		}

		# Strip dates from the title
		if ($title =~ /(.*?), \d+\/\d+\/\d+/) {
			$title = $1;
		}

		$log->is_info && $log->info("$title $weekdays[$wday] $hour:$min $entry->{url}");
		
# Determine the program parent PID  (i.e. not the pid attribute in the entry but separate piod element) as XMLin puts borth pids into an array.
# Second entry is usually what we want.
		$log->error("Entry array should have just two pids ".scalar (@{$entry->{'pid'}}) ) unless (scalar (@{$entry->{'pid'}}) == 2) ;
		my $program_pid = $entry->{'pid'}[1];
#		parents_pid is set if program is part of Brand with many epsiodes.  0 if it si not in which case needs to use the program pid.

		my $series_content ;
		my $brand_content  ;
		my $brand_pid   = 0;
		my $parents_pid = 0;
		
		if ((exists ( $entry->{'brand'} ))) {
			$parents_pid    = $entry->{'parents'} -> {'Brand'}->{'pid'};
			$brand_pid = $parents_pid;
			$brand_content  = $entry->{'brand'};
		} ;

		if ((exists ( $entry->{'series'} ))) {
			$parents_pid    = $entry->{'parents'} -> {'Series'}->{'pid'};
			$series_content = $entry->{'series'};
		} ;
		
		my $version_pid = $entry->{'broadcast'}->{'version_pid'};
		$log->warn("No version_pid for $title !") unless $version_pid;

		if ( $program_pid eq $version_pid ) {
			$program_pid = $entry->{'pid'}[0];
		} ;
		
		# group by key - brand/series/title
		my $key;
		if    ($entry->{'brand'} ) { $key = $entry->{'brand'};   }
		elsif ($entry->{'series'}) { $key = $entry->{'series'};  }
		else                       { $key = $title;              }
			# Note that we needed to quote the value supplied to the filter, because it is expected to be a
			# perl regex. Usually of no consequence, but occasionally titles show up containing punctuation.			
	
		my $icon;
		$icon = $entry->{'images'}->{'image'}->{'content'}             if ( exists($entry->{'images'}->{'image'}->{'content'}) ) ;
		$icon = $entry->{'images'}->{'image'}->{'series'}->{'content'} if ( exists($entry->{'images'}->{'image'}->{'series'}->{'content'}))  ;
		$icon = $entry->{'images'}->{'image'}->{'brand'}->{'content'}  if ( exists($entry->{'images'}->{'image'}->{'brand'}->{'content'}))  ;

		$icon =~ s/ichef.bbci.co.uk\/images\/ic\/\d+x\d+\//ichef.bbci.co.uk\/images\/ic\/$imageres\//;

		# The programme pid should be the ideal key, but that's lost in an array of
		# two pids due to the way the XML is built and XMLin processes it.
		# So we use 'broadcast->version_pid' as our key, which is part of the
		# stream selector, easily retrieved, and just as good.
		# Actually, it might even be better ! E.g. "In Our Time" is repeated, but the
		# repeat is a shortened version. Same programme pid, but different version_pid.

		my $version_pid = $entry->{'broadcast'}->{'version_pid'};
		$log->warn("No version_pid for $title !") unless $version_pid;

		# Test for a version pid, in case xml changes and breaks things...
		unless ($version_pid && $programmeSeen{$version_pid}) {
			
			# Construct a textual part of the 'byKey' title by eliminating its first part,
			# which will be identical to '$key'.
			# e.g. "John Finnemore's Souvenir Programme: Series 4: Episode 5" has
			# Brand "John Finnemore's Souvenir Programme" and Series "Series 4". In this
			# case $key would be the brand.

			my $byKeyTitle = $title;
			$byKeyTitle =~ s/^\Q${key}\E:\s*//g;
			
			if ($parents_pid) {
				
				$byKey{$key} =       {
					'name'        => $key, 
					'url'         => "http://www.bbc.co.uk/programmes/$parents_pid/episodes/player?page=1",
					'icon'        => $icon,
					'parser'      => "Plugins::BBCiPlayerExtra::ExtraPlayerParser",
					'_brandpid'    => $brand_pid,
					'_brandtitle'  => defined($brand_content)  ? $brand_content  : ' ',
					'_seriestitle' => defined($series_content) ? $series_content : ' ',
					} ;
			} else {
				$byKey{$key} = {
					'name'        => $key, 
					'url'         => "http://www.bbc.co.uk/programmes/$program_pid/playlist.json",
					'icon'        => $icon,
					'type'        => 'playlist',
					'description' => $entry->{'synopsis'},
					'on_select'   => 'play',
					'parser'      => 'Plugins::BBCiPlayerExtra::ExtraPlayableParser',					
					} ;
			} ;

			$programmeSeen{$version_pid} = 1 if $version_pid;
		} else {
			$log->is_info && $log->info("Supressing repeat: $title");
		} ;
	}

# create menu

	my @menu;
	# create the by brand/series/title menu entries, promoting single entrys to top level

	for my $key (sort keys %byKey) {
			push @menu,$byKey{$key} ;
	}

	# return xmlbrowser hash
	return {
		'name'    => $params->{'feedTitle'} . " ",
		'items'   => \@menu,
		'type'    => 'opml',
#		'nocache' => 1,
		
	};
}

1;
