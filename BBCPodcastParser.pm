package Plugins::BBCiPlayerExtra::BBCPodcastParser;

use strict;

use Slim::Utils::Log;

use XML::Simple;
use Date::Parse;
use URI::Escape qw(uri_escape_utf8 uri_unescape);

use Data::Dumper;

my $log = logger('plugin.bbciplayerextra');

my %bbcstationslist = (
 'radio1'                       => { 'priority' =>  1, 'fullname' => 'BBC Radio 1'},
 '1xtra'                        => { 'priority' =>  2, 'fullname' => 'BBC Radio 1xtra'},
 'radio2'                       => { 'priority' =>  3, 'fullname' => 'BBC Radio 2'},
 'radio3'                       => { 'priority' =>  4, 'fullname' => 'BBC Radio 3'},
 'radio4'                       => { 'priority' =>  5, 'fullname' => 'BBC Radio 4'},
 'radio4extra'                  => { 'priority' =>  6, 'fullname' => 'BBC Radio 4 Extra'},
 '5live'                        => { 'priority' =>  7, 'fullname' => 'BBC Radio 5 live'},
 '5livesportsextra'             => { 'priority' =>  8, 'fullname' => 'BBC Radio 5 live Extra'},
 '6music'                       => { 'priority' =>  9, 'fullname' => 'BBC Radio 6 Music'},
 'worldserviceradio'            => { 'priority' => 10, 'fullname' => 'BBC World Service'}, 
 'bbc_sounds_podcasts'          => { 'priority' => 11, 'fullname' => 'BBC Sounds podcasts'},
 'cbeebiesradio'                => { 'priority' => 12, 'fullname' => 'CBeebies Radio '},
 'asiannetwork'                 => { 'priority' => 13, 'fullname' => 'Asian Network'},
 'radioscotland'                => { 'priority' => 14, 'fullname' => 'BBC Radio Scotland'},
 'radiowales'                   => { 'priority' => 15, 'fullname' => 'BBC Radio Wales'},
 'radioulster'                  => { 'priority' => 16, 'fullname' => 'BBC Radio Ulster'},
 'radiofoyle'                   => { 'priority' => 17, 'fullname' => 'BBC Radio Foyle'},
 'radiocymru'                   => { 'priority' => 18, 'fullname' => 'BBC Radio Cymru'},
 'radiocymru2'                  => { 'priority' => 19, 'fullname' => 'BBC Radio Cymru2'},
 'radiocymrumwy'                => { 'priority' => 20, 'fullname' => 'BBC Radio Cymruwy'},
 'radionangaidheal'             => { 'priority' => 21, 'fullname' => 'BBC Radio na Gaidheal'},
 'radio7'                       => { 'priority' => 22, 'fullname' => 'BBC Radio 7'},
 'bbc_local_radio'              => { 'priority' => 23, 'fullname' => 'BBC Local Radio'},
 'bbc_two'                      => { 'priority' => 24, 'fullname' => 'BBC Two'},
 'bbc_radio_webonly'            => { 'priority' => 25, 'fullname' => 'BBC Web only'},
 'local'                        => { 'priority' => 26, 'fullname' => 'BBC Local '},
 'bbccoventryandwarwickshire'   => { 'priority' => 27, 'fullname' => 'BBC Radio Coventry & Warwickshire'},
 'bbcessex'                     => { 'priority' => 28, 'fullname' => 'BBC Radio Essex'},
 'bbcherefordandworcester'      => { 'priority' => 29, 'fullname' => 'BBC Radio Hereford & Worcester'},
 'bbcnewcastle'                 => { 'priority' => 30, 'fullname' => 'BBC Radio Newcastle'},
 'bbcsomerset'                  => { 'priority' => 31, 'fullname' => 'BBC Radio Somerset'},
 'bbcsurrey'                    => { 'priority' => 32, 'fullname' => 'BBC Radio Surrey'},
 'bbcsussex'                    => { 'priority' => 33, 'fullname' => 'BBC Radio Sussex'},
 'bbctees'                      => { 'priority' => 34, 'fullname' => 'BBC Radio Tees'},
 'bbcwiltshire'                 => { 'priority' => 35, 'fullname' => 'BBC Wiltshire'},
 'radioberkshire'               => { 'priority' => 36, 'fullname' => 'BBC Radio Berskire'},
 'radiobristol'                 => { 'priority' => 37, 'fullname' => 'BBC Radio Bristol'},
 'radiocambridgeshire'          => { 'priority' => 38, 'fullname' => 'BBC Radio Cambridgeshire'},
 'radiocornwall'                => { 'priority' => 39, 'fullname' => 'BBC Radio Cornwall'},
 'radiocumbria'                 => { 'priority' => 40, 'fullname' => 'BBC Radio Cumbria'},
 'radioderby'                   => { 'priority' => 41, 'fullname' => 'BBC Radio Derby'},
 'radiodevon'                   => { 'priority' => 42, 'fullname' => 'BBC Radio Devon'},
 'radiogloucestershire'         => { 'priority' => 43, 'fullname' => 'BBC Radio Gloucestershire'},
 'radioguernsey'                => { 'priority' => 44, 'fullname' => 'BBC Radio Guernsey'},
 'radiohumberside'              => { 'priority' => 45, 'fullname' => 'BBC Radio Humberside'},
 'radiojersey'                  => { 'priority' => 46, 'fullname' => 'BBC Radio Jersey'},
 'radiokent'                    => { 'priority' => 47, 'fullname' => 'BBC Radio Kent'},
 'radiolancashire'              => { 'priority' => 48, 'fullname' => 'BBC Radio Lancashire'},
 'radioleeds'                   => { 'priority' => 49, 'fullname' => 'BBC Radio Leeds'},
 'radioleicester'               => { 'priority' => 50, 'fullname' => 'BBC Radio Leicester'},
 'radiolincolnshire'            => { 'priority' => 51, 'fullname' => 'BBC Radio Lincolnshire'},
 'radiolondon'                  => { 'priority' => 52, 'fullname' => 'BBC Radio London'},
 'radiomanchester'              => { 'priority' => 53, 'fullname' => 'BBC Radio Manchester'},
 'radiomerseyside'              => { 'priority' => 54, 'fullname' => 'BBC Radio Merseyside'},
 'radionorfolk'                 => { 'priority' => 55, 'fullname' => 'BBC Radio Norfolk'},
 'radionorthampton'             => { 'priority' => 56, 'fullname' => 'BBC Radio Northampton'},
 'radionottingham'              => { 'priority' => 57, 'fullname' => 'BBC Radio Nottingham'},
 'radiooxford'                  => { 'priority' => 58, 'fullname' => 'BBC Radio Oxford'},
 'radiosheffield'               => { 'priority' => 59, 'fullname' => 'BBC Radio Sheffield'},
 'radioshropshire'              => { 'priority' => 60, 'fullname' => 'BBC Radio Shropshire'},
 'radiosolent'                  => { 'priority' => 61, 'fullname' => 'BBC Radio Solent'},
 'radiostoke'                   => { 'priority' => 62, 'fullname' => 'BBC Radio Stoke'},
 'radiosuffolk'                 => { 'priority' => 63, 'fullname' => 'BBC Radio Suffolk'},
 'radioyork'                    => { 'priority' => 64, 'fullname' => 'BBC Radio York'},
 'threecountiesradio'           => { 'priority' => 65, 'fullname' => 'BBC Three Counties Radio'},
 'wm'                           => { 'priority' => 66, 'fullname' => 'BBC WM 95.6'},
 'radio250s'                    => { 'priority' => 67, 'fullname' => 'BBC Radio 250s'},
 'radio2country'                => { 'priority' => 68, 'fullname' => 'BBC Radio 2 Country'},
 'schoolradio'                  => { 'priority' => 69, 'fullname' => 'School Radio'},
 'bbc_korean_radio'             => { 'priority' => 70, 'fullname' => 'BBC Korean Radio'},
 'bbc_learning_english'         => { 'priority' => 71, 'fullname' => 'BBC Learning English'},
 'bbc_russian_radio'            => { 'priority' => 72, 'fullname' => 'BBC Russian Radio'},
 'afriqueradio'                 => { 'priority' => 73, 'fullname' => 'BBC Afrique Radio'},
 'arabicradio'                  => { 'priority' => 74, 'fullname' => 'BBC Arabic Radio'},
 'banglaradio'                  => { 'priority' => 75, 'fullname' => 'BBC Bangla Radio'},
 'burmeseradio'                 => { 'priority' => 76, 'fullname' => 'BBC Burmese Radio'},
 'cantoneseradio'               => { 'priority' => 77, 'fullname' => 'BBC Cantonese Radio'},
 'dariradio'                    => { 'priority' => 78, 'fullname' => 'BBC Dari Radio'},
 'gahuzaradio'                  => { 'priority' => 79, 'fullname' => 'BBC Gahuza Radio'},
 'hausaradio'                   => { 'priority' => 80, 'fullname' => 'BBC Hausa Radio'},
 'hindiradio'                   => { 'priority' => 81, 'fullname' => 'BBC Hindi Radio'},
 'indonesiaradio'               => { 'priority' => 82, 'fullname' => 'BBC Indonesia Radio'},
 'kyrgyzradio'                  => { 'priority' => 83, 'fullname' => 'BBC Kyrgzr  Radio'},
 'nepaliradio'                  => { 'priority' => 84, 'fullname' => 'BBC Nepali Radio'},
 'pashtoradio'                  => { 'priority' => 85, 'fullname' => 'BBC Pashto Radio'},
 'persianradio'                 => { 'priority' => 86, 'fullname' => 'BBC Persian Radio'},
 'sinhalaradio'                 => { 'priority' => 87, 'fullname' => 'BBC Sinhala Radio'},
 'somaliradio'                  => { 'priority' => 88, 'fullname' => 'BBC Somali Radio'},
 'swahiliradio'                 => { 'priority' => 89, 'fullname' => 'BBC Swahili Radio'},
 'tamilradio'                   => { 'priority' => 90, 'fullname' => 'BBC Tamil Radio'},
 'urduradio'                    => { 'priority' => 91, 'fullname' => 'BBC Urdu Radio'},
 'uzbekradio'                   => { 'priority' => 92, 'fullname' => 'BBC Uzbek Radio'},
 );

sub parse {
    my $class  = shift;
    my $http   = shift;

    my $params = $http->params('params');
    my $url    = $params->{'url'};

	my $podcastlist = XMLin($http->contentRef, forcearray => [ 'outline', 'body' ], SuppressEmpty => undef  );

	if ($@) {
		$log->error("$@");
		return;
	}

	my %bbcgenres;
	my $networkslist = $podcastlist->{'body'}[0]->{'outline'}[0]->{'outline'};

	my @stationsmenu;
	my %genrespresent;

	foreach my $href (@$networkslist) {
		$log->info("Processing ". $href->{'text'}  . " ". $href->{'networkId'});
		my @stationmenu;
		foreach my $podcast (@{$href->{'outline'}}) {
			my @genres = split(', ',$podcast->{'bbcgenres'} );

			push @stationmenu, {
				'name'    =>  $podcast->{'text'},
				'icon'    =>  $podcast->{'imageHref'},
				'url'     =>  $podcast->{'xmlUrl'},
				'parser'  => 'Slim::Plugin::Podcast::Parser',
# Don't include description so that podcast can play on JSON Comet client Touch/radio, Apps & Material skin
#				'description' => $podcast->{'description'},
			};
			$log->info("Station: ". $href->{'networkId'} . "Podcast count ". @stationmenu);
			foreach my $genre (@genres) {
				$genrespresent{$genre} += 1;
				push @{ $bbcgenres{$genre} }, $podcast;
			};
		};
		$log->info("Podcast Genres ". Dumper(\%genrespresent));
		if (@stationmenu > 0) {
			my @sortstationmenu = sort {$a->{'name'} cmp $b->{'name'} } @stationmenu;

			push @stationsmenu, {
				'name'  =>   $bbcstationslist{$href->{'networkId'}}->{'fullname'} . ' ('. @sortstationmenu . ')' ,
				'sortkey' => $bbcstationslist{$href->{'networkId'}}->{'priority'},   # this field is ignore  by LMS but will help sort station menu
				'type'  =>  'opml',
				'items' =>  \@sortstationmenu,
			};	
		};
	};

	my @sortstationsmenu = sort {$a->{'sortkey'} <=> $b->{'sortkey'} } @stationsmenu;
	$log->debug("By Station Menu ". Dumper(\@sortstationsmenu));
	
	my @genresmenu;
	foreach my $genre ((sort keys %genrespresent)) {
		my @submenu;
		foreach my $genreitem (@{ $bbcgenres{$genre} }) {
			push @submenu, {
				'name'    =>  $genreitem->{'text'},
				'icon'    =>  $genreitem->{'imageHref'},
				'url'     =>  $genreitem->{'xmlUrl'},
				'parser'  => 'Slim::Plugin::Podcast::Parser',
# Don't include description so that podcast can play on JSON Comet client Touch/radio, Apps & Material skin
#				'description' => $genreitem->{'description'},
			};
		};
		
		 if (@submenu  > 0) {
			my @sortedsubmenu = sort {lc($a->{'name'}) cmp lc($b->{'name'}) } @submenu;
			push @genresmenu, {
				'name'  =>  $genre . ' ('. @sortedsubmenu . ')',
				'type'  =>  'opml',
				'items' =>  \@sortedsubmenu,
			};
		};
	};

	$log->debug("By Genres Menu ". Dumper(\@genresmenu));

	my @podcastmenu ;
	push @podcastmenu, {
		'name' => 'Podcast by Genre',
		'type' => 'opml',
		'items' => \@genresmenu,
	} ;

	push @podcastmenu, {
		'name' => 'Podcast by Station',
		'type' => 'opml',
		'items' => \@sortstationsmenu,
	};

		
	# return xmlbrowser hash
	return {
		'name'    => $params->{'feedTitle'},
		'items'   => \@podcastmenu,
		'type'    => 'opml',
#		'nocache' => $opts->{'nocache'},
	};
}

1;
