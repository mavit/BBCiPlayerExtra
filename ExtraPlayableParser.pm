package Plugins::BBCiPlayerExtra::ExtraPlayableParser;

use strict;

use JSON::XS::VersionOneAndTwo;
use HTML::Entities;

use Slim::Utils::Log;
use Slim::Utils::Prefs;

use URI::Escape qw(uri_escape_utf8 uri_unescape);

use Data::Dumper;

use constant TTL_EXPIRE => 10;

my $log = logger('plugin.bbciplayer.extra');

my $prefs = preferences('plugin.bbciplayerextra');
my $prefsplayer = preferences('plugin.bbciplayer');

sub parse {
    my $class  = shift;
    my $http   = shift;
    my $params = $http->params('params');
    my $url    = $params->{'url'};

	my $imageres = $prefsplayer->get('imageres'); 		

#
# If got an Mediaselector V6 URL in JSON then finally got all the required info - mainly duration and PID
#

    if ($url =~ m/http:\/\/open\.live\.bbc\.co\.uk\/mediaselector\/6\/select\/version\/2\.0\/vpid\/([a-z0-9]+)\/mediaset\/pc/) {
		my $vpid = $1;
#		$log->error("vpid=$vpid  url=$url");
		$log->info("Extra Playable mtis URL  =". $url);
		$log->debug("Extra Playable mtis URL  =". ${$http->contentRef});

		my $client = $params->{'client'};
	        my $item   = $params->{'item'};
		my $title  = $item->{'streamtitle'} || $item->{'name'} || $item->{'title'} || $params->{'feedTitle'};

		my $icon   = $item->{'icon'};
		my $desc   = $item->{'description'};
		my $dur    = $item->{'dur'};
		my $artist = $item->{'artist'};
		
		

		my $stream;

		my $jsondata = eval { from_json(${$http->contentRef}) };

		if ($@) {
			$log->error("Error parsing JSON data $@");
#			$log->info("Dump raw JSON " . Dumper($http->contentRef));
#			$log->info("Dump parsed JSON " . Dumper($jsondata));
			return;
		}

		$log->debug("dump of Mediaselector V6 JSON ". Dumper($jsondata));

		my @programformats;
		my $maxbitrate = 0;
		foreach my $media (@{$jsondata->{'media'}}) {
			foreach my $connection (@{$media->{'connection'}}) {
				my %entry ;
				$entry{'bitrate'} = int($media->{'bitrate'}); 
				$maxbitrate       = $entry{'bitrate'} if ($entry{'bitrate'} > $maxbitrate); 
				if (($connection->{'protocol'} eq 'http') && ($connection->{'transferFormat'} eq 'dash')) {
					$entry{'transport'} = 'dash';
					$entry{'href'}       = $connection->{'href'};
					$entry{'priority'}   = int($connection->{'priority'});
					push @programformats, \%entry;	
				} elsif (($connection->{'protocol'} eq 'https')) {
					$log->info("ignored - https transport format protocol=\"". $connection->{'protocol'} ."\" transfer format=". $connection->{'transferFormat'});
				} else {
					$log->info("Unsupported transport format protocol=\"". $connection->{'protocol'} ."\" transfer format=". $connection->{'transferFormat'});
				};
			}
		}

		$log->info("Available formats ". Dumper(@programformats));
		
		@programformats = sort {	$a->{'transport'} cmp $b->{'transport'}   or 
									$a->{'bitrate'}   cmp $b->{'bitrate'}     or 
									$a->{'priority'}  cmp $b->{'priority'}   
								} @programformats;

#		$log->debug("Sorted formats ". Dumper(@programformats));

		my $dashurl;

		foreach my $program (@programformats) {
			if ($program->{'transport'} eq 'dash') {
				$dashurl = $program->{'href'};
				$log->debug("dash format Title:$title URL=$dashurl");
				last;
			}
		}
		return unless defined($dashurl);
			
		$stream ="iplayer://aod?dash=$dashurl&dur=$dur&icon=$icon&title=" . uri_escape_utf8($title) . "&desc=" . uri_escape_utf8($desc);

		$log->info("Returning Title:$title URL=$stream");
		
		return {
			'type'  => 'opml',
			'items' => [ {
				'name'   => $title,
				'artist' => $artist,
				'url'  => $stream,
				'type' => 'audio',
				'icon' => $icon,
				'description' => $desc,
			} ],
			'cachetime' => 0,
			'replaceparent' => 1,
		};
    }
   
#
#  Process a JSON Playlist details to get duration and PID needed for MTIS url - 
#

    if ($url =~ m/playlist.json/) {

		$log->info("Extra Playable playlist URL  =". $url );
		my $json = eval { from_json( Encode::decode_utf8(${$http->contentRef} )) };	
		if ($@) {
			$log->error("$@");
			return;
		};
		$log->debug("Dump JSON of URL \n", Dumper($json));

		my $playlistitem = $json->{'defaultAvailableVersion'}->{'smpConfig'};
#		my $title =  $playlistitem->{'title'};

		my $title ;
		if (exists( $params->{'feedTitle'})) {
			$title  = $params->{'feedTitle'};
		} else {
			$title  = $params->{'item'}->{'value'};  # Value is used when selection from a old SB3 type interface
		};
		
		my $icon  =  $playlistitem->{'holdingImageURL'};
		$log->info( "Image URL missing http: $icon") unless ($icon =~m/^http/) ;

	
		$icon =~ s/\$recipe/$imageres/;
		$icon =~ s/^\/\/ichef/http:\/\/ichef/;

		my $desc  =  $playlistitem->{'summary'};
#	BBC sometimes uses wide char quotes - change to ASCII single quote as BBCiPlayer plugin doesn't support wide chars.
		$desc =~ s/\x{2019}/'/g;
		$desc =~ s/\x{2018}/'/g;

		my $radioitem;
		foreach my $item  (@{$playlistitem->{'items'}}) {
			if (($item->{'kind'} eq 'radioProgramme') ||( $item->{'kind'} eq 'programme') ) {
				$radioitem = $item;
				last;
			};
		}
	
		if ( ! defined($radioitem)) {
			$log->error("playlistitem no radioprogramme or programme found \n", Dumper($playlistitem));
		}; 
		my $dur   =  $radioitem->{'duration'};
		my $pid   =  $radioitem->{'vpid'};

		$params->{'item'}->{'name'}        = $title;
		$params->{'item'}->{'streamtitle'} = $title;
		$params->{'item'}->{'feedTitle'}   = $title;
		$params->{'item'}->{'dur'}         = $dur;
		$params->{'item'}->{'description'} = $desc;
		$params->{'item'}->{'artist'}      = $playlistitem->{'service'}->{'content'};
		$params->{'item'}->{'icon'}        = $icon;

		return {
			'type'    => 'redirect',
			'url'     => 'http://open.live.bbc.co.uk/mediaselector/6/select/version/2.0/vpid/'. $pid .'/mediaset/pc/format/json' ,
		};
	}
}


# Local Variables:
# tab-width:4
# indent-tabs-mode:t
# End:

1;
