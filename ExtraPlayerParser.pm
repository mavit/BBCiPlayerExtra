#
#
# This program is free software; you can redistribute it and/or 
# modify it under the terms of the GNU General Public License, version 2.
#
#
package Plugins::BBCiPlayerExtra::ExtraPlayerParser;

use strict;

use Slim::Utils::Log;
use Slim::Utils::Prefs;
use Date::Parse;

use HTML::PullParser;
use JSON::XS::VersionOneAndTwo;
use Encode qw(encode_utf8);

my $log = logger('plugin.bbciplayer.extra');
my $prefsplayer = preferences('plugin.bbciplayer');

use Data::Dumper;

my @months   = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);

sub getnexttag
{
    my $t;
    my $p         = shift;
    my $searchtag = shift;
    my $exittag   = shift ;

    for (;;) {
        $t = $p->get_token();
        return undef unless (defined($t));
        return $t if ( $t->[1] eq $searchtag) ;
        return $t if ( defined ($exittag) && $t->[1] eq $exittag) ;
    }
}

sub getnext2tag
{
  my $t;
  my ($p,$searchtag,$endsearchtag) = @_;

  for (;;) {
     $t = $p->get_token();
     return undef unless (defined($t));
     return $t if ( $t->[1] eq $endsearchtag) ;
     return $t if ( $t->[1] eq $searchtag) ;

  }
}

sub getnext3tag
{
  my $t;
  my ($p,$search1tag,$search2tag,$search3tag,) = @_;

  for (;;) {
     $t = $p->get_token();
     return undef unless (defined($t));
     return $t if ( $t->[1] eq $search1tag) ;
     return $t if ( $t->[1] eq $search2tag) ;
     return $t if ( $t->[1] eq $search3tag) ;
  }
}

sub parse
{
    my $class  = shift;
    my $http   = shift;
    
    my $params = $http->params('params');
    my $url    = $params->{'url'};
    my $client = $params->{'client'};
	my $series_title = $http->params('params')->{'item'}->{'_seriestitle'};
	my $brand_pid    = $http->params('params')->{'item'}->{'_brandpid'};
	my $brand_title  = $http->params('params')->{'item'}->{'_brandtitle'};
	my $top_icon     = $http->params('params')->{'item'}->{'icon'};
	
	my $imageres          = $prefsplayer->get('imageres');
	my $partofradioseries;

    my @menu;
    my $t;
    my $pageno;
    my $maxpages;
    
    if ($url =~ m/player\?page=(\d+)/) {
		$pageno = $1;
		$log->info( "Page No = $pageno");
    };
 
    my $p = HTML::PullParser->new(api_version => 3, doc => Encode::decode_utf8(${$http->contentRef}), 
                                  start => 'event, tag, attr,  skipped_text',
                                  end   => 'event, tag, dtext, skipped_text',
                                  report_tags => [qw ( ol script a )]);

	while (defined($t =  getnext2tag($p,'script','ol'))) {
		if ( ($t->[1] eq 'ol') && ( defined($t->[2]->{'class'} )))  {
			next if ($t->[2]->{'class'} ne 'pagination') ;

			$log->info("Found pagination element ");

#
#  Found pagination section - get the first "Page 2 of" entry to find total numebrof pages  
#
			$t = getnexttag($p,'a');
			$maxpages = 1;
			if ( ($t->[1] eq 'a') && ( defined($t->[2]->{'aria-label'} )))  {
				my $pagestring = $t->[2]->{'aria-label'} ;
				$pagestring  =~ /Page 2 of (\d+)/;
				$maxpages = $1;
				$log->info( "Max Pages = $maxpages");
			}	

# skip past the remaining pagination list elements
			$t = getnexttag($p,'/ol');

# Go find the application+json script element which has list of RadioEpisodes
			$t = getnexttag($p,'script');
			return \@menu unless defined($t) ;
			
		};
#
#  Here is found a script element - now spin until the right one is found
#		
		$log->info("Found script element ");
		
		if ( ($t->[1] eq 'script') && ( defined($t->[2]->{'type'} )))  {
			next if ($t->[2]->{'type'} ne 'application/ld+json') ;

			$t =  getnexttag($p,"/script") ;
			my $script_json = $t->[3] ;
			$log->info("Found JSON script element \n  $script_json");
		
			my $scriptdata = eval { from_json($script_json) };

			if ( $@ || $scriptdata->{error} ) {
				$log->error( "error parsing BBC script json data" . $@ . $scriptdata->{error});
				return \@menu;
			}
			
			$log->info(" scripts JSON type ". $scriptdata->{'@type'} );
			$log->debug(" scripts JSON dump \n". Dumper(\$scriptdata) );
			
			if (($scriptdata->{'@type'}  eq 'RadioSeries' ) || ($scriptdata->{'@type'}  eq 'RadioSeason' )) {
			  my $series_name  = $scriptdata->{'name'};
			  my $series_descr = $scriptdata->{'description'};
			  my $series_icon  = $scriptdata->{'image'};

			  $partofradioseries = $scriptdata->{'partOfSeries'}->{'url'} if ($scriptdata->{'@type'}  eq 'RadioSeason' );
			  
			  my $title_prefix;
			  
			  if ($series_title ne ' ') {
					$title_prefix = $series_title ;
			  } elsif ($brand_title ne ' ') {
					$title_prefix = $brand_title ;
			  } else {
				$title_prefix = $params->{'feedTitle'} ;
			  }
			  
			  foreach my $episode (@{$scriptdata->{'episode'}}) {

					my $icon = $episode->{'image'};
					my $start = str2time($episode->{'datePublished'});
					my ( $day, $mon, $wday) = (localtime($start))[3,4,6];

					push @menu, {
						'name'	      => $title_prefix. ' - ' . $episode->{'name'} . " ($day $months[$mon])" ,
						'icon'        => $icon,
						'url'         => 'http://www.bbc.co.uk/programmes/'. $episode->{'identifier'} . '/playlist.json' ,
						'type'        => 'playlist',
						'on_select'   => 'play',
						'parser'      => 'Plugins::BBCiPlayerExtra::ExtraPlayableParser',			
						'description' => $episode->{'description'} ,
					}				  
			  }
#  Found a Radio Series - now exit the epsidoe list for this page.  Fall down into adding entries for other pages.			  
			  $log->debug(" scripts JSON - processmenu \n". Dumper(\@menu) );
			  last; # exit the while loop processing HTML page
			} 
		} ;
	};

	$log->info(" Found scripts and pages number - finish up ");

#  Having processed all episodes on this page  - if Page 1 of episodes then add menus for other episodes of same series.  
	if ($pageno == 1) {			
	 	(my $rooturl = $url) =~ s/player\?page=1/player/; 
		for (my $i = 2; $i <= $maxpages; $i++) { 
			push @menu, {

						'name'	      => "Page $i - " . $params->{'feedTitle'},
						'url'         => "$rooturl?page=$i",
						'parser'      => "Plugins::BBCiPlayerExtra::ExtraPlayerParser",
						'icon'        => $top_icon,
						'_brandpid'    => $brand_pid,
						'_brandtitle'  => $brand_title ,
						'_seriestitle' => $series_title,	
			};	
		}
	} ;
	
	if (defined($partofradioseries)) {
			push @menu, {
						'name'	      => "Other - " . $params->{'feedTitle'},
						'url'         => "$partofradioseries/episodes/player?page=1",
						'parser'      => "Plugins::BBCiPlayerExtra::ExtraPlayerParser",
						'icon'        => $top_icon,
						'_brandpid'    => 0,
						'_brandtitle'  => ' ' ,
						'_seriestitle' => ' ',					};	
	} ;	
		
	$log->info("Dump of full menu ". Dumper(\@menu));

	# return xmlbrowser hash
	return {
		'type'  => 'opml',
		'title' => $params->{'feedTitle'},
		'items' => \@menu,
	};
}
# Local Variables:
# tab-width:4
# indent-tabs-mode:t
# End:

1;
