#
#
# This program is free software; you can redistribute it and/or 
# modify it under the terms of the GNU General Public License, version 2.
#
#
package Plugins::BBCiPlayerExtra::ExtraProgrammesAtoZParser;

use strict;

use Slim::Utils::Log;
use Slim::Utils::Prefs;
use Date::Parse;

use HTML::PullParser;
use Encode qw(encode_utf8);

my $log = logger('plugin.bbciplayer.extra');
my $prefsplayer = preferences('plugin.bbciplayer');

use Data::Dumper;

sub getnexttag
{
    my $t;
    my $p         = shift;
    my $searchtag = shift;
    my $exittag   = shift ;

    for (;;) {
        $t = $p->get_token();
        return undef unless (defined($t));
        return $t if ( $t->[1] eq $searchtag) ;
        return $t if ( defined ($exittag) && $t->[1] eq $exittag) ;
    }
}

sub getnext2tag
{
  my $t;
  my ($p,$searchtag,$endsearchtag) = @_;

  for (;;) {
     $t = $p->get_token();
     return undef unless (defined($t));
     return $t if ( $t->[1] eq $endsearchtag) ;
     return $t if ( $t->[1] eq $searchtag) ;

  }
}


sub parse
{
    my $class  = shift;
    my $http   = shift;
    
    my $params = $http->params('params');

    my $url          = $params->{'url'};
    my $client       = $params->{'client'};
	my $top_icon     = $params->{'item'}->{'icon'};
	my $menuText     = $params->{'item'}->{'text'};
	
	my $imageres = $prefsplayer->get('imageres');

    my $pageno = 1;
    if ($url =~ m/player\?page=(\d+)/) {
		$pageno = $1;
		$log->debug("Page No = $pageno");
    };

    my @menu;
    my $t;

	my $p = HTML::PullParser->new(api_version => 3, doc => HTML::Entities::decode_entities(Encode::decode_utf8(${$http->contentRef})), 
                                  start => 'event, tag, attr,  skipped_text',
                                  end   => 'event, tag, dtext, skipped_text',
                                  report_tags => [qw ( ol div a li span img p h2)]);
	$log->info(" parsing $url");

#  Find start od list of programmes
	while (defined($t = getnexttag($p,'ol'))) {
		last if (index($t->[2]->{'class'}, 'highlight-box-wrapper') != -1 );
	};

	unless (defined ($t)) {
		$log->error(" Failed initial parsing $url");
		return {
			'type'  => 'opml',
			'title' => 'Failed - ' .$params->{'feedTitle'},
			'items' => [{
				'name'	      => 'Parsing failed - not a good page',
					},],
		};
	}
	
	$log->info(" Found highlight box parsing $url");

#  Loop around - examine each programme in list.  Only use "programme--radio" Radio programmes.  

	while (defined($t =  getnext2tag($p,'li','/ol'))) {
		last if ($t->[1] eq '/ol');
		
		$t = getnexttag($p,'div');
		my $classofprog = $t->[2]->{'class'} ;
		
		next if (index($classofprog, 'programme--radio') == -1);
		my $datapid     = $t->[2]->{'data-pid'} ;
		my $imgsrcurl        = '';
		my $program_title    = '';
		my $program_subtitle = '';
		my $program_type;
		if (    index($classofprog, 'programme--episode') != -1) { $program_type = 'episode' }
		elsif ( index($classofprog, 'programme--series')  != -1) { $program_type = 'series'  }
		elsif ( index($classofprog, 'programme--brand')   != -1) { $program_type = 'brand'   }
		else { $program_type = 'unknown' };
		
#		Select one of the programme image for the LMS icon 
		
		while(defined($t = getnexttag($p,"div"))) {
			last if ($t->[2]->{'class'} eq 'programme__body');
			if (index($t->[2]->{'class'},'programme__img--hasimage') != -1) {
				$t = getnexttag($p,'img');
				$imgsrcurl = $t->[2]->{'data-src'}
			};
		}

#  At the program main set of detail in div class='programme-body'	for LMS title & description
#  h2 has the program details in subsequent span

		next unless (defined($t = getnexttag($p,'h2')));
		$t =  getnexttag($p,'/span') ;
		$program_title = $t->[3] ;		

		next unless (defined($t = getnexttag($p,'p')));
		if (index($t->[2]->{'class'},'programme__synopsis') != -1) {
			$t =  getnexttag($p,'/span') ;
			$program_subtitle = $t->[3] ;		
		}
		
#		$log->error("Pid $datapid $program_type Title: $program_title  Synopsis: $program_subtitle   Image URL $imgsrcurl\n");
		if ($program_title eq '') {
				$log->error("Program $datapid  - blank title - omitted "); 
				next ;
		}

#  If episode - then link is immediately playable.		
		if ( $program_type eq 'episode') {
			push @menu, {
					'name'	      => $program_title,
					'icon'        => $imgsrcurl,
					'url'         => "http://www.bbc.co.uk/programmes/$datapid/playlist.json" ,
					'type'        => 'playlist',
					'on_select'   => 'play',
					'parser'      => 'Plugins::BBCiPlayerExtra::ExtraPlayableParser',			
			}	
#  If series or brand then link is to multiple episodes so make a menu with another parser with submenu of many episodes.			
		} elsif ( $program_type eq 'series' || $program_type eq 'brand')  {
			push @menu, {
					'name'	      => $program_title,
					'icon'        => $imgsrcurl,
					'url'         => "http://www.bbc.co.uk/programmes/$datapid/episodes/player?page=1",
					'parser'      => "Plugins::BBCiPlayerExtra::ExtraPlayerParser",
					'_brandpid'    => 0,
					'_brandtitle'  => ' ' ,
					'_seriestitle' => ' ',					
			}	
#  Catch any odd things - maybe new BBC classification			
		} else {
		   $log->error("Omitting unknown program $datapid $program_title");	
		} ;
	} ;


#  Having processed all items - if Page 1 of genre then add menus for other pages in same genre.  
#  First find out how many pages.
	if ($pageno == 1) {
		$t = getnexttag($p,'li');
		if (($t->[1] eq 'li') && (index($t->[2]->{'class'},'pagination__previous') != -1)) {
			$t = getnexttag($p,'li');
			$t = getnexttag($p,'a');
			$t->[2]->{'aria-label'}  =~ /Page 2 of (\d+)/;
			my $totalpages = $1;
			$log->debug("\nTotal pages = $totalpages");
			(my $rooturl = $url) =~ s/player\?page=1/player/; 
			
			for (my $i = 2; $i <= $totalpages; $i++) { 
				push @menu, {
					'name'	      => "Page $i - " . $params->{'feedTitle'},
					'url'         => "$rooturl?page=$i",
					'parser'      => "Plugins::BBCiPlayerExtra::ExtraProgrammesAtoZParser",
				};	
			}
		}
	}

# All done 
	if (scalar @menu == 0) {
		push @menu , {
			'name'	      => 'No programs found',
		} ;
	}

	$log->debug("Dump of menu ". Dumper(\@menu));

	# return xmlbrowser hash
	return {
		'type'  => 'opml',
		'title' => $params->{'feedTitle'},
		'items' => \@menu,
	};
}

1;
