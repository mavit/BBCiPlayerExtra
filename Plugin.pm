package Plugins::BBCiPlayerExtra::Plugin;

# Plugin to add menu to BBCiPlayer 
#
# Released under GPLv2

use strict;

use base qw(Slim::Plugin::OPMLBased);


use File::Spec::Functions qw(:ALL);
use Plugins::BBCiPlayerExtra::Settings;
use Slim::Utils::Prefs;

my $prefs = preferences('plugin.bbciplayerextra');
my $playerprefs = preferences('plugin.bbciplayer');

my $log = Slim::Utils::Log->addLogCategory({
	'category'     => 'plugin.bbciplayer.extra',
	'defaultLevel' => 'ERROR',
	'description'  => getDisplayName(),
});

$prefs->migrate(2, sub {
	$prefs->set('disabledash', 0);
	1;
});

sub initPlugin {
	my $class = shift;

	my $file = catdir( $class->_pluginDataFor('basedir'), 'default.opml' );
	
	$prefs->init({ disabledash => 0 });
	
	Slim::Menu::TrackInfo->registerInfoProvider( moresegmentsinfo => (
		before => 'bottom',
		func   => \&Plugins::BBCiPlayer::Plugin::segmentsInfoMenu,
	) );

	$class->SUPER::initPlugin(
		feed   => Slim::Utils::Misc::fileURLFromPath($file),
		tag    => 'bbciplayerextra',
		is_app => $class->can('nonSNApps') && $playerprefs->get('is_app') ? 1 : undef,
		menu   => 'radios',
		weight => 1,
	);

	Plugins::BBCiPlayerExtra::Settings->new;

}

sub getDisplayName { 'PLUGIN_BBCIPLAYEREXTRA' }


sub playerMenu { shift->can('nonSNApps') && $playerprefs->get('is_app') ? undef : 'RADIO' }


1;
