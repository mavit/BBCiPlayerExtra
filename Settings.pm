package Plugins::BBCiPlayerExtra::Settings;

use strict;
use base qw(Slim::Web::Settings);

use Slim::Utils::Prefs;

sub name {
	return 'PLUGIN_BBCIPLAYEREXTRA';
}

sub page {
	return 'plugins/BBCiPlayerExtra/settings/basic.html';
}

sub prefs {
	return (preferences('plugin.bbciplayerextra'), qw( disabledash));
}


1;
